
"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

from util import *
from logic import *


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
        state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
        actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def miniWumpusSearch(problem):
    """
    A sample pass through the miniWumpus layout. Your solution will not contain
    just three steps! Optimality is not the concern here.
    """
    from game import Directions
    e = Directions.EAST
    n = Directions.NORTH
    return [e, n, n]


def logicBasedSearch(problem):
    """

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())

    print "Does the Wumpus's stench reach my spot?",
               \ problem.isWumpusClose(problem.getStartState())

    print "Can I sense the chemicals from the pills?",
               \ problem.isPoisonCapsuleClose(problem.getStartState())

    print "Can I see the glow from the teleporter?",
               \ problem.isTeleporterClose(problem.getStartState())

    (the slash '\\' is used to combine commands spanning through multiple lines -
    you should remove it if you convert the commands to a single line)

    Feel free to create and use as many helper functions as you want.

    A couple of hints:
        * Use the getSuccessors method, not only when you are looking for states
        you can transition into. In case you want to resolve if a poisoned pill is
        at a certain state, it might be easy to check if you can sense the chemicals
        on all cells surrounding the state.
        * Memorize information, often and thoroughly. Dictionaries are your friends and
        states (tuples) can be used as keys.
        * Keep track of the states you visit in order. You do NOT need to remember the
        tranisitions - simply pass the visited states to the 'reconstructPath' method
        in the search problem. Check logicAgents.py and search.py for implementation.
    """
    # array in order to keep the ordering

    visitedStates = []
    startState = problem.getStartState()
    visitedStates.append(startState)
    data = {}
    safeStates = PriorityQueue()

    current = startState
    data[current] = Labels.SAFE
    safeStates.push(startState, 20 * startState[0] + startState[1])

    while not safeStates.isEmpty():

        current = safeStates.pop()
        visitedStates.append(current)
        sensedSmell = False
        sensedPoison = False
        sensedGlow = False

        print "Visiting", current
        if problem.isWumpusClose(current):
            print "sensed s", current
            sensedSmell = True
        else:
            sensedSmell = False
            print "sensed ~s", current
        if problem.isPoisonCapsuleClose(current):
            sensedPoison = True
            print "sensed b", current
        else:
            print "sensed ~b", current
            sensedPoison = False
        if problem.isTeleporterClose(current):
            sensedGlow = True
            print "sensed g", current
        else:
            print "sensed ~g", current
            sensedGlow = False

        if(problem.isGoalState(current)):
            return problem.reconstructPath(visitedStates)

        for succ, dir, cost in problem.getSuccessors(current):

            wumpus = False
            poison = False
            teleporter = False

            if conclusion1(current, succ, problem):
                data[succ] = Labels.WUMPUS
                print "concluded w", succ
                wumpus = True
            elif not sensedSmell:
                wumpus = False
                print "concluded ~w", succ

            if conclusion2(current, succ, problem):
                data[succ] = Labels.POISON
                print "concluded p", succ
                poison = True
            elif not sensedPoison:
                poison = False
                print "concluded ~p", succ

            if conclusion3(current, succ, problem):
                data[succ] = Labels.TELEPORTER
                "concluded t", succ
                teleporter = True
            elif not sensedGlow:
                teleporter = False
                print "concluded ~t", succ

            if ((not wumpus) and (not poison)) or teleporter:
                print "concluded o", succ
                data[succ] = Labels.SAFE
                if succ not in visitedStates:
                    safeStates.push(succ, 20 * succ[0] + succ[1])
            else:
                print "concluded ~o", succ

    return []


def conclusion1(state, succ, problem):
    goal = Clause([Literal(Labels.WUMPUS, succ)])
    premises1 = rule1(state, problem)
    premises2 = rule6(state, problem)
    premises3 = rule8(state, problem)

    return resolution(premises1, goal) or resolution(premises2, goal) or resolution(premises3, goal)


def conclusion2(state, succ, problem):
    goal = Clause([Literal(Labels.POISON, succ)])
    premises1 = rule2(state, problem)
    premises2 = rule3(state, problem)
    premises3 = rule8(state, problem)

    return resolution(premises1, goal) or resolution(premises2, goal) or resolution(premises3, goal)


def conclusion3(state, succ,  problem):
    goal = Clause([Literal(Labels.TELEPORTER, succ)])
    premises1 = rule4(state, problem)
    premises2 = rule5(state, problem)

    return resolution(premises1, goal) or resolution(premises2, goal)


def conclusion4(state, succ, problem):
    goal = Clause([Literal(Labels.SAFE, succ)])
    premises1 = rule7(state, problem)
    premises2 = rule8(state, problem)

    return resolution(premises1, goal) or resolution(premises2, goal)


def rule1(state, problem):
    premises = set()
    for succ, dir, cost in problem.getSuccessors(state):
        premise = Clause(set([Literal(Labels.WUMPUS_STENCH, state), Literal(
            Labels.WUMPUS, succ, negative=True)]))
        premises.add(premise)

    return premises


def rule2(state, problem):
    premises = set()
    literals = []

    literals.append(Literal(Labels.POISON_FUMES, state, negative=True))
    for succ, dir, cost in problem.getSuccessors(state):
        literals.append(Literal(Labels.POISON, succ, ))

    premises.add(Clause(set(literals)))
    return premises


def rule3(state, problem):
    premises = set()

    for succ, dir, cost in problem.getSuccessors(state):
        premises.add(Clause(set([Literal(Labels.POISON_FUMES, state), Literal(
            Labels.POISON, succ, negative=True)])))

    return premises


def rule4(state, problem):
    premises = set()

    literals = []
    literals.append(Literal(Labels.TELEPORTER_GLOW, state, negative=True))
    for succ, dir, cost in problem.getSuccessors(state):
        literals.append(Literal(Labels.TELEPORTER, succ))

    premises.add(Clause(set(literals)))
    return premises


def rule5(state, problem):
    premises = set()

    for succ, dir, cost in problem.getSuccessors(state):
        premises.add(Clause((set([Literal(Labels.TELEPORTER_GLOW, state), Literal(
            Labels.TELEPORTER, succ, negative=True)]))))

    return premises


def rule6(state, problem):
    premises = set()

    for succ, dir, cost in problem.getSuccessors(state):
        premises.add(Clause([Literal(Labels.WUMPUS, state),
                             Literal(Labels.WUMPUS, succ, negative=True)]))

    return premises


def rule7(state, problem):
    premises = set()

    for succ, dir, cost in problem.getSuccessors:
        premises.add(Clause([Literal(Labels.POISON_FUMES, state), Literal(
            Labels.WUMPUS_STENCH, state), Literal(Labels.SAFE, succ)]))

    return premises


def rule8(state, problem):
    premises = set()

    premises.add(Clause([Literal(Labels.WUMPUS, state), Literal(
        Labels.POISON_FUMES, state), Literal(Labels.SAFE, state)]))

    return premises


# Abbreviations
lbs = logicBasedSearch
