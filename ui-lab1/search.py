
# -*- coding: utf-8 -*-
# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
import copy
from game import Directions


class SearchNode:
    """
    This class represents a node in the graph which represents the search problem.
    The class is used as a basic wrapper for search methods - you may use it, however
    you can solve the assignment without it.

    REMINDER: You need to fill in the backtrack function in this class!
    """

    def __init__(self, position, parent=None, transition=None, cost=0, heuristic=0):
        """
        Basic constructor which copies the values. Remember, you can access all the
        values of a python object simply by referencing them - there is no need for
        a getter method.
        """
        self.position = position
        self.parent = parent
        self.cost = cost
        self.heuristic = heuristic
        self.transition = transition

    def isRootNode(self):
        """
        Check if the node has a parent.
        returns True in case it does, False otherwise
        """
        return self.parent == None

    def unpack(self):
        """
        Return all relevant values for the current node.
        Returns position, parent node, cost, heuristic value
        """
        return self.position, self.parent, self.cost, self.heuristic

    def getPosition(self):
        return self.position

    def calculateDirection(self, nextPos):
        x1 = self.position[0]
        y1 = self.position[1]
        x2 = nextPos[0]
        y2 = nextPos[1]

        if x1 == x2:
            if y2 > y1:
                return Directions.NORTH

            return Directions.SOUTH

        elif x2 > x1:
            return Directions.EAST

        else:
            return Directions.WEST

    def backtrack(self):
        """
        Reconstruct a path to the initial state from the current node.
        Bear in mind that usually you will reconstruct the path from the
        final node to the initial.
        """
        moves = []
        # make a deep copy to stop any referencing isues.
        node = copy.deepcopy(self)

        if node.isRootNode():
            # The initial state is the final state
            return moves

        "**YOUR CODE HERE**"
        while(not node.isRootNode()):
            moves.insert(0, node.transition)
            node = node.parent

        return moves

    def __eq__(self, other):
        if other is None:
            return False
        if (self.position != other.position):
            return False
        if (self.cost != other.cost):
            return False
        if(self.heuristic != other.heuristic):
            return False

        return True


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    s = Directions.SOUTH
    w = Directions.WEST

    return [s, s, w, s, w, w, s, w]


def blindSearch(problem, dataStructure, insertFunction):
    """
    The general implementation of blind search algorithms.
    The user needs to provide a method (insertFunction) to specify
    the order of searching that defines the specified algorithm.
    """
    open = dataStructure
    visited = []
    goal = None

    insertFunction(open, SearchNode(
        position=problem.getStartState(), parent=None), problem)

    # open.push(SearchNode(position=problem.getStartState(), parent=None))

    while (not open.isEmpty()):
        n = open.pop()

        if n.getPosition() in visited:
            continue

        if problem.isGoalState(n.getPosition()):
            goal = n
            break

        visited.append(n.getPosition())
        for m in problem.getSuccessors(n.getPosition()):
            if m[0] not in visited:
                insertFunction(open, SearchNode(
                    position=m[0], parent=n, transition=m[1]), problem)

    if goal is not None:
        return goal.backtrack()

    return []


def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """

    def insertFunction(dataStructure, node, problem):
        dataStructure.push(node)

    return blindSearch(problem, util.Stack(), insertFunction)


def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"

    def insertFunction(dataStructure, node, problem):
        dataStructure.push(node)

    return blindSearch(problem, util.Queue(), insertFunction)


def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR FRIEND'S CODE HERE ***"

    def insertFunction(dataStructure, node, problem):
        dataStructure.push(node, problem.getCostOfActions(node.backtrack()))

    return blindSearch(problem, util.PriorityQueue(), insertFunction)


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0


def aStarSearch(problem, heuristic=nullHeuristic):
    open = util.PriorityQueue()
    start = SearchNode(problem.getStartState())
    open.push(start, heuristic(problem.getStartState(), problem))
    visited = []
    costDict = {}
    costDict[start.position] = 0

    while not open.isEmpty():
        current = open.pop()
        if problem.isGoalState(current.position):
            return current.backtrack()

        visited.append(current.position)
        for succ in problem.getSuccessors(current.position):
            state, move, cost = succ[0], succ[1], succ[2]

            if(state not in visited and checkCostHigher(current, succ, costDict, open)):
                newCost = current.cost + cost
                costDict[state] = newCost
                newNode = SearchNode(
                    position=state, parent=current, cost=newCost, transition=move)
                open.push(newNode, heuristic(
                    newNode.position, problem) + newCost)

    return []


def checkCostHigher(current, succ, costDict, open):
    for n in open.heap:
        if n[2].position == succ[0]:
            if (n[2].position in costDict.keys()):
                if costDict[n[2].position] <= (current.cost + succ[2]):
                    return False

    return True


def checkClosed(succ, closed):
    for m in closed:
        if m.getPosition() == succ[0]:
            if m[2].cost < succ[2]:
                return True
            else:
                closed.remove(m)


def checkOpen(succ, open):
    for m in open.heap:
        if m[2].getPosition() == succ[0]:
            if m[2].cost < succ[2]:
                return True
            else:
                open.heap.remove(m)


def removeFromQueue(item, queue):
    for tmp in queue.heap:
        if tmp[2] == item and (tmp[0] == (item.cost + item.heuristic)):
            queue.heap.remove(tmp)


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
