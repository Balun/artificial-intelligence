"""
Search the deepest nodes in the search tree first.

Your search algorithm needs to return a list of actions that reaches the
goal. Make sure to implement a graph search algorithm.

To get started, you might want to try some of these simple commands to
understand the search problem that is being passed in:

print "Start:", problem.getStartState()
print "Is the start a goal?", problem.isGoalState(problem.getStartState())
print "Start's successors:", problem.getSuccessors(problem.getStartState())
"""


def insertFunction(dataStructure, node, problem):
    dataStructure.push(node)


return blindSearch(problem, util.Stack(), insertFunction)


def breadthFirstSearch(problem):

"""Search the shallowest nodes in the search tree first."""
"*** YOUR CODE HERE ***"


def insertFunction(dataStructure, node, problem):
    dataStructure.push(node)


return blindSearch(problem, util.Queue(), insertFunction)


def uniformCostSearch(problem):

"""Search the node of least total cost first."""
"*** YOUR CODE HERE ***"


def insertFunction(dataStructure, node, problem):
    dataStructure.push(node, problem.getCostOfActions(node.backtrack()))


return blindSearch(problem, util.PriorityQueue(), insertFunction)


def nullHeuristic(state, problem=None):

"""
A heuristic function estimates the cost from the current state to the nearest
goal in the provided SearchProblem.  This heuristic is trivial.
"""
return 0


def aStarSearch(problem, heuristic=nullHeuristic):

"""Search the node that has the lowest combined cost and heuristic first."""
"*** YOUR CODE HERE ***"

open = util.PriorityQueue()
closed = []
goal = None

start = SearchNode(position=problem.getStartState(),
                   parent=None, heuristic=heuristic(problem.getStartState(), problem), cost=0)
open.push(item=start, priority=start.cost + start.heuristic)

while(not open.isEmpty()):
    n = open.pop()
    if problem.isGoalState(n.getPosition()):
        goal = n
        break

    closed.append(n)
    for m in problem.getSuccessors(n.getPosition()):
        node = SearchNode(position=m[0], parent=n,
                          transition=m[1], heuristic=heuristic(m[0], problem), cost=n.cost + 1)
        tmp = containsByState(open, closed, m)
        if tmp is not None:

            if problem.getCostOfActions(tmp.backtrack()) < problem.getCostOfActions(node.backtrack()):
                continue
            else:
                if tmp in closed:
                    closed.remove(tmp)
                removeFromQueue(tmp, open)

        open.push(item=node, priority=node.cost + node.heuristic)

if goal is not None:
    return goal.backtrack()

return []


def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"

    open = util.PriorityQueue()
    closed = []
    visitedStates = []
    goal = None

    start = SearchNode(position=problem.getStartState(),
                       parent=None, heuristic=heuristic(problem.getStartState(), problem), cost=0)
    open.push(item=start, priority=start.cost + start.heuristic)

    while(not open.isEmpty()):
        n = open.pop()
        if problem.isGoalState(n.getPosition()):
            goal = n
            break

        closed.append(n)
        visitedStates.append(n.getPosition())
        for succ in problem.getSuccessors(n.getPosition()):
            if not succ[0] in visitedStates:
                if checkOpen(succ, open):
                    continue
                if checkClosed(succ, closed):
                    continue

                node = SearchNode(
                    position=succ[0], parent=n, cost=n.cost + 1, heuristic=heuristic(succ[0], problem), transition=succ[1])
                open.push(node, node.heuristic + node.cost)

    if goal is not None:
        return goal.backtrack()

    return []


def aStarSearch(problem, heuristic=nullHeuristic):
    open = util.PriorityQueue()
    start = SearchNode(problem.getStartState())
    open.push(start, heuristic(problem.getStartState(), problem))
    visited = []
    costDict = {}
    costDict[start.position] = 0

    while not open.isEmpty():
        current = open.pop()
        if problem.isGoalState(current.position):
            return current.backtrack()

        visited.append(current.position)
        for e in problem.getSuccessors(current.position):
            state, move, cost = e[0], e[1], e[2]
            higher = True
            for n in open.heap:
                if n[2].position == state:
                    if (n[2].position in costDict.keys()):
                        if costDic[[n2].position] > (current.cost + cost):
                            higher = True
                        else:
                            higher = False

                        break

            if(state not in visited and higher):
                costDict[state] = current.cost + cost
                if state not in visited:
                    newCost = current.cost + cost
                    newNode = SearchNode(
                        position=state, parent=current, cost=newCost, transition=move)
                    open.push(newNode, heuristic(
                        newNode.position, problem) + newCost)
